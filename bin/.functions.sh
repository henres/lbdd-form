#!/usr/bin/env bash

CURRENT_BASH=$(ps -p $$ | awk '{ print $4}' | tail -n 1)
case "$CURRENT_BASH" in
	-zsh|zsh)
		CURRENT_DIR=$( cd "$( dirname $0 )" && pwd )
		;;
	bash)
		CURRENT_DIR=$( cd "$( dirname ${BASH_SOURCE[0]} )" && pwd )
		;;
	*)
		echo 1>&2
		echo -e "\033[0;31m\`${CURRENT_BASH}\` does not seems to be supported\033[0m" 1>&2
		echo 1>&2
		return 1
		;;
esac

sed -e 's/^/export /' "${CURRENT_DIR}/../.env"  > "${CURRENT_DIR}/../.env.sh"
source "${CURRENT_DIR}/../.env.sh" > /dev/null
rm "${CURRENT_DIR}/../.env.sh"

unalias dc 2> /dev/null > /dev/null || true
dc() {
	docker-compose -p ${APP_PROJECT_NAME} "$@"
}
export -f dc

unalias dc-down 2> /dev/null > /dev/null || true
dc-down() {
	printf "\n>>> Docker compose stop and destroy\n\n" && \
	dc down
}
export -f dc-down

unalias dc-up 2> /dev/null > /dev/null || true
dc-up() {
	printf "\n>>> Docker up with no deamon\n\n" && \
	dc up
}
export -f dc-up

unalias dc-upd 2> /dev/null > /dev/null || true
dc-upd() {
	printf "\n>>> Docker up with deamon\n\n" && \
	dc up -d
}
export -f dc-upd

unalias dc-exec 2> /dev/null > /dev/null || true
dc-exec() {
	printf "\n>>> Docker compose exec\n\n" && \
	dc exec "$@"
}
export -f dc-exec

unalias php 2> /dev/null > /dev/null || true
php() {
	dc exec php_fpm sh -c "php $*"
}
export -f php

unalias composer 2> /dev/null > /dev/null || true
composer() {
	dc exec php_fpm sh -c "composer $*"
}
export -f composer

unalias console 2> /dev/null > /dev/null || true
console() {
	php bin/console "$@"
}
export -f console

unalias phpunit 2> /dev/null > /dev/null || true
phpunit() {
	php vendor/bin/phpunit "$@"
}
export -f phpunit

unalias sf-check 2> /dev/null > /dev/null || true
sf-check() {
	php bin/symfony_requirements
}
export -f sf-check

unalias dev 2> /dev/null > /dev/null || true
dev() {
	console --env=dev "$@"
}
export -f dev

unalias prod 2> /dev/null > /dev/null || true
prod() {
	console --env=prod "$@"
}
export -f prod

unalias migration 2> /dev/null > /dev/null || true
migration() {
	console --env=migration "$@"
}
export -f migration

unalias pgsql 2> /dev/null > /dev/null || true
pgsql() {
	dc exec postgres sh -c "psql -U lbdd -d lbdd $*"
}
export -f pgsql

#unalias redis 2> /dev/null > /dev/null || true
#redis() {
#	docker exec -it ${ACCOUNT_MANAGER_API_PREFIX}_redis sh -c "redis-cli $*"
#}
#export -f redis
