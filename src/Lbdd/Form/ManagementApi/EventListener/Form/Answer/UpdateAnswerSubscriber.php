<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Answer;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Answer\UpdateAnswerPersister;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\UpdateFormPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateAnswerSubscriber implements EventSubscriberInterface
{
    /**
     * @var UpdateAnswerPersister
     */
    private $updateAnswerPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * UpdateAnswerSubscriber constructor.
     *
     * @param UpdateAnswerPersister $updateAnswerPersister
     * @param SerializerInterface   $serializer
     */
    public function __construct(UpdateAnswerPersister $updateAnswerPersister, SerializerInterface $serializer)
    {
        $this->updateAnswerPersister = $updateAnswerPersister;
        $this->serializer            = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'update',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function update(GetResponseForControllerResultEvent $event): void
    {
        $answer = $event->getControllerResult();

        if (
            !$answer instanceof Answer
            || 'api_form_question_answer_put_subresource' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->updateAnswerPersister->persist($answer),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_OK
            )
        );
    }
}
