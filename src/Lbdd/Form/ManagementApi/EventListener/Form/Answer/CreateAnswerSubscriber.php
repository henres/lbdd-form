<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Answer;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Answer\CreateAnswerPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class CreateAnswerSubscriber implements EventSubscriberInterface
{
    /**
     * @var CreateAnswerPersister
     */
    private $createAnswerPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * CreateAnswerSubscriber constructor.
     *
     * @param CreateAnswerPersister $createAnswerPersister
     * @param SerializerInterface   $serializer
     */
    public function __construct(CreateAnswerPersister $createAnswerPersister, SerializerInterface $serializer)
    {
        $this->createAnswerPersister = $createAnswerPersister;
        $this->serializer            = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'create',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function create(GetResponseForControllerResultEvent $event): void
    {
        $answer = $event->getControllerResult();

        if (
            !$answer instanceof Answer
            || 'api_form_questions_answers_post_subresource' !== $event->getRequest()->get('_api_collection_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->createAnswerPersister->persist($answer),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_CREATED
            )
        );
    }
}
