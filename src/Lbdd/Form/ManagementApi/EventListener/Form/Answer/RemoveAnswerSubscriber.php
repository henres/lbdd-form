<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Answer;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Answer\RemoveAnswerPersister;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\RemoveFormPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class RemoveAnswerSubscriber implements EventSubscriberInterface
{
    /**
     * @var RemoveAnswerPersister
     */
    private $removeAnswerPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * RemoveAnswerSubscriber constructor.
     *
     * @param RemoveAnswerPersister $removeAnswerPersister
     * @param SerializerInterface   $serializer
     */
    public function __construct(RemoveAnswerPersister $removeAnswerPersister, SerializerInterface $serializer)
    {
        $this->removeAnswerPersister = $removeAnswerPersister;
        $this->serializer            = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'remove',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function remove(GetResponseForControllerResultEvent $event): void
    {
        $answer = $event->getControllerResult();

        if (
            !$answer instanceof Answer
            || 'api_form_question_answer_delete_subresource' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $this->removeAnswerPersister->persist($answer);
    }
}
