<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Form;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\UpdateFormPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var UpdateFormPersister
     */
    private $updateFormPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * UpdateFormSubscriber constructor.
     *
     * @param UpdateFormPersister $updateFormPersister
     * @param SerializerInterface $serializer
     */
    public function __construct(UpdateFormPersister $updateFormPersister, SerializerInterface $serializer)
    {
        $this->updateFormPersister = $updateFormPersister;
        $this->serializer          = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'update',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function update(GetResponseForControllerResultEvent $event): void
    {
        $form = $event->getControllerResult();

        if (
            !$form instanceof Form
            || 'put' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->updateFormPersister->persist($form),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_OK
            )
        );
    }
}
