<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Form;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\CreateFormPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class CreateFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var CreateFormPersister
     */
    private $createFormPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * CreateInvitationSubscriber constructor.
     *
     * @param CreateFormPersister $createFormPersister
     * @param SerializerInterface $serializer
     */
    public function __construct(CreateFormPersister $createFormPersister, SerializerInterface $serializer)
    {
        $this->createFormPersister = $createFormPersister;
        $this->serializer          = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'create',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function create(GetResponseForControllerResultEvent $event): void
    {
        $form = $event->getControllerResult();

        if (
            !$form instanceof Form
            || 'post' !== $event->getRequest()->get('_api_collection_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->createFormPersister->persist($form),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_CREATED
            )
        );
    }
}
