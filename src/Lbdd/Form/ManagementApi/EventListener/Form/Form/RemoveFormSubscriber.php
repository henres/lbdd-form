<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Form;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\RemoveFormPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class RemoveFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var RemoveFormPersister
     */
    private $removeFormPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * RemoveFormSubscriber constructor.
     *
     * @param RemoveFormPersister $removeFormPersister
     * @param SerializerInterface $serializer
     */
    public function __construct(RemoveFormPersister $removeFormPersister, SerializerInterface $serializer)
    {
        $this->removeFormPersister = $removeFormPersister;
        $this->serializer          = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'remove',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function remove(GetResponseForControllerResultEvent $event): void
    {
        $form = $event->getControllerResult();

        if (
            !$form instanceof Form
            || 'delete' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $this->removeFormPersister->persist($form);
    }
}
