<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Question;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Question\UpdateQuestionPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateQuestionsFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var UpdateQuestionPersister
     */
    private $updateQuestionPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * UpdateQuestionsFormSubscriber constructor.
     *
     * @param UpdateQuestionPersister $updateQuestionPersister
     * @param SerializerInterface     $serializer
     */
    public function __construct(UpdateQuestionPersister $updateQuestionPersister, SerializerInterface $serializer)
    {
        $this->updateQuestionPersister = $updateQuestionPersister;
        $this->serializer               = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'getQuestions',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function getQuestions(GetResponseForControllerResultEvent $event): void
    {
        $question = $event->getControllerResult();

        if (
            !$question instanceof Question
            || 'api_form_question_put_subresource' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->updateQuestionPersister->persist($question),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_OK
            )
        );
    }
}
