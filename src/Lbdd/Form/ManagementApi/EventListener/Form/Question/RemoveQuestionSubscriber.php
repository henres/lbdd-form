<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Question;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Form\RemoveFormPersister;
use Lbdd\Form\ManagementApi\DataPersister\Form\Question\RemoveQuestionPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class RemoveQuestionSubscriber implements EventSubscriberInterface
{
    /**
     * @var RemoveQuestionPersister
     */
    private $removeQuestionPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * RemoveQuestionSubscriber constructor.
     *
     * @param RemoveQuestionPersister $removeQuestionPersister
     * @param SerializerInterface     $serializer
     */
    public function __construct(RemoveQuestionPersister $removeQuestionPersister, SerializerInterface $serializer)
    {
        $this->removeQuestionPersister = $removeQuestionPersister;
        $this->serializer              = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'remove',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function remove(GetResponseForControllerResultEvent $event): void
    {
        $form = $event->getControllerResult();

        if (
            !$form instanceof Question
            || 'delete' !== $event->getRequest()->get('_api_item_operation_name')
        ) {
            return;
        }

        $this->removeQuestionPersister->persist($form);
    }
}
