<?php

namespace Lbdd\Form\ManagementApi\EventListener\Form\Question;

use ApiPlatform\Core\EventListener\EventPriorities;
use Lbdd\Form\ManagementApi\DataPersister\Form\Question\CreateQuestionPersister;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class CreateQuestionsFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var CreateQuestionPersister
     */
    private $createQuestionPersister;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * CreateQuestionsFormSubscriber constructor.
     *
     * @param CreateQuestionPersister $createQuestionPersister
     * @param SerializerInterface     $serializer
     */
    public function __construct(
        CreateQuestionPersister $createQuestionPersister,
        SerializerInterface $serializer
    ) {
        $this->createQuestionPersister = $createQuestionPersister;
        $this->serializer              = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [
                    'getQuestions',
                    EventPriorities::PRE_WRITE,
                ],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function getQuestions(GetResponseForControllerResultEvent $event): void
    {
        $question = $event->getControllerResult();

        if (
            !$question instanceof Question
            || 'api_form_questions_post_subresource' !== $event->getRequest()->get('_api_collection_operation_name')
        ) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->serializer->serialize(
                    $this->createQuestionPersister->persist($question),
                    $event->getRequest()->getRequestFormat()
                ),
                Response::HTTP_OK
            )
        );
    }
}
