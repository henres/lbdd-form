<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Question;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\ManagementApi\Query\Form\Question\GetQuestionsQuery;
use Lbdd\Form\ManagementApi\Query\Form\Question\GetQuestionsQueryHandler;
use Symfony\Component\HttpFoundation\RequestStack;

final class QuestionCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetQuestionsQueryHandler
     */
    private $getQuestionsQueryHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AnswerCollectionDataProvider constructor.
     *
     * @param GetQuestionsQueryHandler $getQuestionsQueryHandler
     * @param RequestStack             $requestStack
     */
    public function __construct(GetQuestionsQueryHandler $getQuestionsQueryHandler, RequestStack $requestStack)
    {
        $this->getQuestionsQueryHandler = $getQuestionsQueryHandler;
        $this->requestStack             = $requestStack;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Question::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null)
    {

        $questionQuery = new GetQuestionsQuery();
        $questionQuery->formId = $this->requestStack->getCurrentRequest()->get('form_id');

        return $this->getQuestionsQueryHandler->handle($questionQuery);
    }
}
