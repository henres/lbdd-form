<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Question;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\ManagementApi\Query\Form\Question\GetQuestionQuery;
use Lbdd\Form\ManagementApi\Query\Form\Question\GetQuestionQueryHandler;
use Symfony\Component\HttpFoundation\RequestStack;

final class QuestionItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetQuestionQueryHandler
     */
    private $getQuestionHandlerQuery;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AnswerItemDataProvider constructor.
     *
     * @param GetQuestionQueryHandler $getQuestionHandlerQuery
     * @param RequestStack            $requestStack
     */
    public function __construct(GetQuestionQueryHandler $getQuestionHandlerQuery, RequestStack $requestStack)
    {
        $this->getQuestionHandlerQuery = $getQuestionHandlerQuery;
        $this->requestStack            = $requestStack;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Question::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): Question
    {
        $formId = $this->requestStack->getCurrentRequest()->get('form_id');

        $getQuestionQuery     = new GetQuestionQuery();
        $getQuestionQuery->id = $id;
        $getQuestionQuery->formId = $formId;

        return $this->getQuestionHandlerQuery->handle($getQuestionQuery);
    }
}
