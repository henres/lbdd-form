<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Answer;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\ManagementApi\Query\Form\Answer\GetAnswersQuery;
use Lbdd\Form\ManagementApi\Query\Form\Answer\GetAnswersQueryHandler;
use Symfony\Component\HttpFoundation\RequestStack;

final class AnswerCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetAnswersQueryHandler
     */
    private $getAnswersQueryHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AnswerCollectionDataProvider constructor.
     *
     * @param GetAnswersQueryHandler $getAnswersQueryHandler
     * @param RequestStack           $requestStack
     */
    public function __construct(GetAnswersQueryHandler $getAnswersQueryHandler, RequestStack $requestStack)
    {
        $this->getAnswersQueryHandler = $getAnswersQueryHandler;
        $this->requestStack           = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Answer::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null)
    {

        $answersQuery = new GetAnswersQuery();
        $answersQuery->questionId = $this->requestStack->getCurrentRequest()->get('question_id');

        return $this->getAnswersQueryHandler->handle($answersQuery);
    }
}
