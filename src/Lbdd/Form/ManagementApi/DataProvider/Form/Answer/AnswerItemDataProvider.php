<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Answer;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\ManagementApi\Query\Form\Answer\GetAnswerQuery;
use Lbdd\Form\ManagementApi\Query\Form\Answer\GetAnswerQueryHandler;
use Symfony\Component\HttpFoundation\RequestStack;

final class AnswerItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetAnswerQueryHandler
     */
    private $getAnswerQueryHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AnswerItemDataProvider constructor.
     *
     * @param GetAnswerQueryHandler $getAnswerQueryHandler
     * @param RequestStack          $requestStack
     */
    public function __construct(GetAnswerQueryHandler $getAnswerQueryHandler, RequestStack $requestStack)
    {
        $this->getAnswerQueryHandler = $getAnswerQueryHandler;
        $this->requestStack          = $requestStack;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Answer::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): Answer
    {
        $questionId = $this->requestStack->getCurrentRequest()->get('question_id');

        $getAnswerQuery             = new GetAnswerQuery();
        $getAnswerQuery->id         = $id;
        $getAnswerQuery->questionId = $questionId;

        return $this->getAnswerQueryHandler->handle($getAnswerQuery);
    }
}
