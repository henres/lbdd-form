<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Form;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\ManagementApi\Query\Form\Form\GetFormQuery;
use Lbdd\Form\ManagementApi\Query\Form\Form\GetFormQueryHandler;

final class FormItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetFormQueryHandler
     */
    private $getFormQueryHandler;

    /**
     * FormItemDataProvider constructor.
     *
     * @param GetFormQueryHandler $getFormQueryHandler
     */
    public function __construct(GetFormQueryHandler $getFormQueryHandler)
    {
        $this->getFormQueryHandler = $getFormQueryHandler;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Form::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): Form
    {
        $getFormQuery     = new GetFormQuery();
        $getFormQuery->id = $id;

        return $this->getFormQueryHandler->handle($getFormQuery);
    }
}
