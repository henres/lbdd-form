<?php

namespace Lbdd\Form\ManagementApi\DataProvider\Form\Form;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\ManagementApi\Query\Form\Form\GetFormsQuery;
use Lbdd\Form\ManagementApi\Query\Form\Form\GetFormsQueryHandler;

final class FormCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var GetFormsQueryHandler
     */
    private $getFormsQueryHandler;

    /**
     * FormCollectionDataProvider constructor.
     *
     * @param GetFormsQueryHandler $getFormsQueryHandler
     */
    public function __construct(GetFormsQueryHandler $getFormsQueryHandler)
    {
        $this->getFormsQueryHandler = $getFormsQueryHandler;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Form::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null)
    {
        $formsQuery = new GetFormsQuery();

        return $this->getFormsQueryHandler->handle($formsQuery);
    }
}
