<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Form;

use Lbdd\Form\Storage\Form\FormRepository;

class RemoveFormCommandHandler
{
    /**
     * @var FormRepository
     */
    private $formRepository;

    /**
     * RemoveFormCommandHandler constructor.
     *
     * @param FormRepository $formRepository
     */
    public function __construct(FormRepository $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * @param RemoveFormCommand $removeFormCommand
     */
    public function handle(RemoveFormCommand $removeFormCommand): void
    {
        $formStorage = $this->formRepository->getOne($removeFormCommand->id);

        $this->formRepository->delete($formStorage);
    }
}
