<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Form;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form as FormStorage;
use Lbdd\Form\Storage\Form\FormRepository;

class CreateFormCommandHandler
{
    /**
     * @var FormRepository
     */
    private $formRepository;

    /**
     * CreateFormCommandHandler constructor.
     *
     * @param FormRepository $formRepository
     */
    public function __construct(FormRepository $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * @param CreateFormCommand $createFormCommand
     *
     * @return Form
     */
    public function handle(CreateFormCommand $createFormCommand): Form
    {
        $formStorage = new FormStorage();
        $formStorage->setTitle($createFormCommand->title);
        $slugify = new Slugify();
        $formStorage->setName($slugify->slugify($formStorage->getTitle()));

        $this->formRepository->save($formStorage);

        $form        = new Form();
        $form->id    = $formStorage->getId();
        $form->title = $formStorage->getTitle();
        $form->name  = $formStorage->getName();

        return $form;
    }
}
