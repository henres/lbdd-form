<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Form;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\Storage\Form\FormRepository;

class UpdateFormCommandHandler
{
    /**
     * @var FormRepository
     */
    private $formRepository;

    /**
     * CreateFormCommandHandler constructor.
     *
     * @param FormRepository $formRepository
     */
    public function __construct(FormRepository $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * @param UpdateFormCommand $updateFormCommand
     *
     * @return Form
     */
    public function handle(UpdateFormCommand $updateFormCommand): Form
    {
        $formStorage = $this->formRepository->getOne($updateFormCommand->id);

        $slugify = new Slugify();
        $formStorage->setTitle($updateFormCommand->title);
        $formStorage->setName($slugify->slugify($formStorage->getTitle()));

        $this->formRepository->save($formStorage);

        $form        = new Form();
        $form->id    = $formStorage->getId();
        $form->name  = $formStorage->getName();
        $form->title = $formStorage->getTitle();

        return $form;
    }
}
