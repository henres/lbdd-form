<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Question;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\Storage\Form\QuestionRepository;

class UpdateQuestionCommandHandler
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * UpdateQuestionCommandHandler constructor.
     *
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param UpdateQuestionCommand $updateQuestionCommand
     *
     * @return Question
     */
    public function handle(UpdateQuestionCommand $updateQuestionCommand): Question
    {
        $questionStorage = $this->questionRepository->getOneByForm($updateQuestionCommand->id, $updateQuestionCommand->formId);

        $slugify = new Slugify();
        $questionStorage->setTitle($updateQuestionCommand->title);
        $questionStorage->setName($slugify->slugify($questionStorage->getTitle()));

        $this->questionRepository->save($questionStorage);

        $question         = new Question();
        $question->id     = $questionStorage->getId();
        $question->name   = $questionStorage->getName();
        $question->title  = $questionStorage->getTitle();
        $question->formId = $questionStorage->getForm()->getId();

        return $question;
    }
}
