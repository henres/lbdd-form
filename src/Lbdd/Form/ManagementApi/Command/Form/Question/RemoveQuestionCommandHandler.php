<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Question;

use Lbdd\Form\Storage\Form\FormRepository;
use Lbdd\Form\Storage\Form\QuestionRepository;

class RemoveQuestionCommandHandler
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * RemoveQuestionCommandHandler constructor.
     *
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param RemoveQuestionCommand $removeQuestionCommand
     */
    public function handle(RemoveQuestionCommand $removeQuestionCommand): void
    {
        $formStorage = $this->questionRepository->getOneByForm($removeQuestionCommand->id, $removeQuestionCommand->formId);

        $this->questionRepository->delete($formStorage);
    }
}
