<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Question;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Question as QuestionStorage;
use Lbdd\Form\Storage\Form\FormRepository;
use Lbdd\Form\Storage\Form\QuestionRepository;

class CreateQuestionCommandHandler
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @var FormRepository
     */
    private $formRepository;

    /**
     * CreateQuestionCommandHandler constructor.
     *
     * @param QuestionRepository $questionRepository
     * @param FormRepository     $formRepository
     */
    public function __construct(QuestionRepository $questionRepository, FormRepository $formRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->formRepository     = $formRepository;
    }

    /**
     * @param CreateQuestionCommand $createQuestionCommand
     *
     * @return Question
     */
    public function handle(CreateQuestionCommand $createQuestionCommand): Question
    {
        $form = $this->formRepository->getOne($createQuestionCommand->formId);

        $slugify = new Slugify();

        $questionStorage = new QuestionStorage();
        $questionStorage->setTitle($createQuestionCommand->title);
        $questionStorage->setName($slugify->slugify($questionStorage->getTitle()));
        $questionStorage->setForm($form);

        $this->questionRepository->save($questionStorage);

        $question         = new Question();
        $question->id     = $questionStorage->getId();
        $question->title  = $questionStorage->getTitle();
        $question->name   = $questionStorage->getName();
        $question->formId = $questionStorage->getForm()->getId();

        return $question;
    }
}
