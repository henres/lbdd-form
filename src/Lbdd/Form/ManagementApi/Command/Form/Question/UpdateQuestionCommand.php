<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Question;

class UpdateQuestionCommand
{
    public $id;

    public $formId;

    public $title;
}
