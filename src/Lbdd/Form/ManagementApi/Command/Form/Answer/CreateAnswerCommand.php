<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Answer;

class CreateAnswerCommand
{
    public $value;

    public $formId;

    public $questionId;

    public $name;
}
