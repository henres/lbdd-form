<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Answer;

class UpdateAnswerCommand
{
    public $id;

    public $questionId;

    public $value;
}
