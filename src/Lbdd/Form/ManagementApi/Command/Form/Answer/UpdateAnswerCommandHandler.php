<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Answer;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\Storage\Form\AnswerRepository;
use Lbdd\Form\Storage\Form\QuestionRepository;

class UpdateAnswerCommandHandler
{
    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * UpdateAnswerCommandHandler constructor.
     *
     * @param AnswerRepository   $answerRepository
     * @param QuestionRepository $questionRepository
     */
    public function __construct(AnswerRepository $answerRepository, QuestionRepository $questionRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    /**
     * @param UpdateAnswerCommand $updateAnswerCommand
     */
    public function handle(UpdateAnswerCommand $updateAnswerCommand)
    {
        $answerStorage = $this->answerRepository->getOneByQuestion($updateAnswerCommand->id, $updateAnswerCommand->questionId);

        $slugify = new Slugify();
        $answerStorage->setValue($updateAnswerCommand->value);
        $answerStorage->setName($slugify->slugify($answerStorage->getValue()));

        $this->answerRepository->save($answerStorage);
    }
}
