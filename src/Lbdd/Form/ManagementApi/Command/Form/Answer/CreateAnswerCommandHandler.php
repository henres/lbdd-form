<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Answer;

use Cocur\Slugify\Slugify;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\Storage\Form\AnswerRepository;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Answer as AnswerStorage;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Question;
use Lbdd\Form\Storage\Form\QuestionRepository;

class CreateAnswerCommandHandler
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * CreateAnswerCommandHandler constructor.
     *
     * @param QuestionRepository $questionRepository
     * @param AnswerRepository   $answerRepository
     */
    public function __construct(QuestionRepository $questionRepository, AnswerRepository $answerRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->answerRepository   = $answerRepository;
    }

    /**
     * @param CreateAnswerCommand $createAnswerCommand
     *
     * @return Answer
     */
    public function handle(CreateAnswerCommand $createAnswerCommand): Answer
    {
        /** @var Question $question */
        $question = $this->questionRepository->getOneByForm($createAnswerCommand->questionId, $createAnswerCommand->formId);

        $slugify = new Slugify();

        $answerStorage = new AnswerStorage();
        $answerStorage->setValue($createAnswerCommand->value);
        $answerStorage->setName($slugify->slugify($answerStorage->getValue()));
        $answerStorage->setQuestion($question);

        $this->answerRepository->save($answerStorage);

        $answer                    = new Answer();
        $answer->id                = $answerStorage->getId();
        $answer->name              = $answerStorage->getName();
        $answer->value             = $answerStorage->getValue();
        $answer->questionId        = $answerStorage->getQuestion()->getId();
        $answer->redirectToForm    = null === $answerStorage->getRedirectToProduct()
                                        ? null : $answerStorage->getRedirectToForm()->getId();
        $answer->redirectToProduct = $answerStorage->getRedirectToProduct();

        return $answer;
    }
}
