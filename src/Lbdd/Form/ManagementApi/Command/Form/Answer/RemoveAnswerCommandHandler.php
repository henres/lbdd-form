<?php

namespace Lbdd\Form\ManagementApi\Command\Form\Answer;

use Lbdd\Form\Storage\Form\AnswerRepository;

class RemoveAnswerCommandHandler
{
    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * RemoveAnswerCommandHandler constructor.
     *
     * @param AnswerRepository $answerRepository
     */
    public function __construct(AnswerRepository $answerRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    /**
     * @param RemoveAnswerCommand $removeAnswerCommand
     */
    public function handle(RemoveAnswerCommand $removeAnswerCommand): void
    {
        $answerStorage = $this->answerRepository->getOneByQuestion($removeAnswerCommand->id, $removeAnswerCommand->questionId);

        $this->answerRepository->delete($answerStorage);
    }
}
