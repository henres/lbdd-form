<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Question;

class GetQuestionsQuery
{
    public $formId;
    public $page;
}