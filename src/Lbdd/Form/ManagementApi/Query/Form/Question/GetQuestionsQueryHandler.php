<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Question;

use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Question as QuestionStorage;
use Lbdd\Form\Storage\Form\QuestionRepository;
use Lbdd\Form\Storage\Form\QuestionRepository as QuestionStorageRepository;

class GetQuestionsQueryHandler
{
    /**
     * @var QuestionStorageRepository
     */
    private $questionStorageRepository;

    /**
     * GetQuestionsQueryHandler constructor.
     *
     * @param QuestionRepository $questionStorageRepository
     */
    public function __construct(QuestionRepository $questionStorageRepository)
    {
        $this->questionStorageRepository = $questionStorageRepository;
    }

    /**
     * @param GetQuestionsQuery $getQuestionsQuery
     *
     * @return array
     */
    public function handle(GetQuestionsQuery $getQuestionsQuery): array
    {
        $questionsStorage = $this->questionStorageRepository->getAllByForm($getQuestionsQuery->formId);

        $questions = \array_map(function (QuestionStorage $questionStorage) {
            $question         = new Question();
            $question->id     = $questionStorage->getId();
            $question->name   = $questionStorage->getName();
            $question->title  = $questionStorage->getTitle();
            $question->formId = $questionStorage->getForm()->getId();

            return $question;
        }, $questionsStorage);

        return $questions;
    }
}
