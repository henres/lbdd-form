<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Question;

class GetQuestionQuery
{
    public $id;
    public $formId;
}