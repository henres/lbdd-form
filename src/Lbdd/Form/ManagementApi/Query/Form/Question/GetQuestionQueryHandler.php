<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Question;

use GetFormQuery;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Lbdd\Form\ManagementApi\Exception\Form\QuestionNotFoundException;
use Lbdd\Form\Storage\Form\QuestionRepository as QuestionStorageRepository;
use Pichet\Component\Exception\ResourceNotFoundException;
use Pichet\ManagementApi\Response\UserResponse;
use Pichet\Storage\User\Doctrine\Entity\User;

class GetQuestionQueryHandler
{
    /**
     * @var QuestionStorageRepository
     */
    private $questionStorageRepository;

    /**
     * GetQuestionQueryHandler constructor.
     *
     * @param QuestionStorageRepository $questionStorageRepository
     */
    public function __construct(QuestionStorageRepository $questionStorageRepository)
    {
        $this->questionStorageRepository = $questionStorageRepository;
    }

    /**
     * @param GetQuestionQuery $getQuestionQuery
     *
     * @return Question
     */
    public function handle(GetQuestionQuery $getQuestionQuery): Question
    {
        $questionStorage = $this->questionStorageRepository->getOneByForm(
            $getQuestionQuery->id,
            $getQuestionQuery->formId
        );

        if (null === $questionStorage) {
            throw new QuestionNotFoundException(sprintf(
                'The question "%s" does not exist in form "%s".',
                $getQuestionQuery->id,
                $getQuestionQuery->formId
            ));
        }

        $question         = new Question();
        $question->id     = $questionStorage->getId();
        $question->name   = $questionStorage->getName();
        $question->title  = $questionStorage->getTitle();
        $question->formId = $questionStorage->getForm()->getId();

        return $question;
    }
}
