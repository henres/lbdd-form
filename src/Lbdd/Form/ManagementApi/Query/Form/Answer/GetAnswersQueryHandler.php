<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Answer;

use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\Storage\Form\AnswerRepository as AnswerStorageRepository;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Answer as AnswerStorage;

class GetAnswersQueryHandler
{
    /**
     * @var AnswerStorageRepository
     */
    private $answerStorageRepository;

    /**
     * GetAnswersQueryHandler constructor.
     *
     * @param AnswerStorageRepository $answerStorageRepository
     */
    public function __construct(AnswerStorageRepository $answerStorageRepository)
    {
        $this->answerStorageRepository = $answerStorageRepository;
    }

    /**
     * @param GetAnswersQuery $getAnswersQuery
     *
     * @return array
     */
    public function handle(GetAnswersQuery $getAnswersQuery): array
    {
        $answersStorage = $this->answerStorageRepository->getAllByQuestion($getAnswersQuery->questionId);

        $answers = \array_map(function (AnswerStorage $answerStorage) {
            $answer             = new Answer();
            $answer->id         = $answerStorage->getId();
            $answer->name       = $answerStorage->getName();
            $answer->value      = $answerStorage->getValue();
            $answer->questionId = $answerStorage->getQuestion()->getId();

            return $answer;
        }, $answersStorage);

        return $answers;
    }
}
