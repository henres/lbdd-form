<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Answer;

use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Lbdd\Form\Storage\Form\AnswerRepository as AnswerStorageRepository;

class GetAnswerQueryHandler
{
    /**
     * @var AnswerStorageRepository
     */
    private $answerStorageRepository;

    /**
     * GetUserQueryHandler constructor.
     *
     * @param AnswerStorageRepository $answerStorageRepository
     */
    public function __construct(AnswerStorageRepository $answerStorageRepository)
    {
        $this->answerStorageRepository = $answerStorageRepository;
    }

    /**
     * @param GetAnswerQuery $getAnswerQuery
     *
     * @return Answer
     * @throws \Lbdd\Form\ManagementApi\Exception\Form\AnswerNotFoundException
     */
    public function handle(GetAnswerQuery $getAnswerQuery): Answer
    {
        $answerStorage = $this->answerStorageRepository->getOneByQuestion($getAnswerQuery->id, $getAnswerQuery->questionId);

        $answer = new Answer();

        $answer->id         = $answerStorage->getId();
        $answer->value      = $answerStorage->getValue();
        $answer->name       = $answerStorage->getName();
        $answer->questionId = $answerStorage->getQuestion()->getId();

        return $answer;
    }
}
