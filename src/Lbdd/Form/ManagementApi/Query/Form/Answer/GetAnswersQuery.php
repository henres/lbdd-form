<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Answer;

class GetAnswersQuery
{
    public $questionId;

    public $page;
}