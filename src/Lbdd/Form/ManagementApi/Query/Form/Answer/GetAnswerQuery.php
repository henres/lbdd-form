<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Answer;

class GetAnswerQuery
{
    public $id;
    public $questionId;
}