<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Form;

use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\ManagementApi\Exception\Form\FormNotFoundException;
use Lbdd\Form\Storage\Form\FormRepository as FormStorageRepository;

class GetFormQueryHandler
{
    /**
     * @var FormStorageRepository
     */
    private $formStorageRepository;

    /**
     * GetUserQueryHandler constructor.
     *
     * @param FormStorageRepository $formStorageRepository
     */
    public function __construct(FormStorageRepository $formStorageRepository)
    {
        $this->formStorageRepository = $formStorageRepository;
    }

    /**
     * @param GetFormQuery $getFormQuery
     *
     * @return Form
     */
    public function handle(GetFormQuery $getFormQuery): Form
    {
        $formStorage = $this->formStorageRepository->getOne($getFormQuery->id);

        $form        = new Form();
        $form->id    = $formStorage->getId();
        $form->name  = $formStorage->getName();
        $form->title = $formStorage->getTitle();

        return $form;
    }
}
