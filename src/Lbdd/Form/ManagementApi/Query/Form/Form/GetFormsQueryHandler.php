<?php

namespace Lbdd\Form\ManagementApi\Query\Form\Form;

use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form as FormStorage;
use Lbdd\Form\Storage\Form\FormRepository as FormStorageRepository;

class GetFormsQueryHandler
{
    /**
     * @var FormStorageRepository
     */
    private $formStorageRepository;

    /**
     * GetAnswersQueryHandler constructor.
     *
     * @param FormStorageRepository $formStorageRepository
     */
    public function __construct(FormStorageRepository $formStorageRepository)
    {
        $this->formStorageRepository = $formStorageRepository;
    }

    /**
     * @param GetFormsQuery $getFormsQuery
     *
     * @return array
     */
    public function handle(GetFormsQuery $getFormsQuery): array
    {
        $formsStorage = $this->formStorageRepository->getAll();

        $forms = \array_map(function (FormStorage $formStorage) {
            $form = new Form();
            $form->id = $formStorage->getId();
            $form->name = $formStorage->getName();
            $form->title = $formStorage->getTitle();

            return $form;
        }, $formsStorage);

        return $forms;
    }
}
