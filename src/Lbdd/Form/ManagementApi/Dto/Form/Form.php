<?php

namespace Lbdd\Form\ManagementApi\Dto\Form;

use ApiPlatform\Core\Annotation\ApiProperty;

class Form
{
    /**
     * @ApiProperty(identifier=true)
     *
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $title;
}