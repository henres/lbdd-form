<?php

namespace Lbdd\Form\ManagementApi\Dto\Form;

use ApiPlatform\Core\Annotation\ApiProperty;

class Question
{
    /**
     * @ApiProperty(identifier=true)
     *
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $formId;
}