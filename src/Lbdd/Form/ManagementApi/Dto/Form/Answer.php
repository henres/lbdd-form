<?php

namespace Lbdd\Form\ManagementApi\Dto\Form;

use ApiPlatform\Core\Annotation\ApiProperty;

class Answer
{
    /**
     * @ApiProperty(identifier=true)
     *
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $redirectToForm;

    /**
     * @var string
     */
    public $redirectToProduct;

    /**
     * @var string
     */
    public $questionId;
}