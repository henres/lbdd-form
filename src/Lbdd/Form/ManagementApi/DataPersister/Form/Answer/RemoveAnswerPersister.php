<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Answer;

use Lbdd\Form\ManagementApi\Command\Form\Answer\RemoveAnswerCommand;
use Lbdd\Form\ManagementApi\Command\Form\Answer\RemoveAnswerCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;

class RemoveAnswerPersister
{
    /**
     * @var RemoveAnswerCommandHandler
     */
    private $removeAnswerCommandHandler;



    /**
     * RemoveAnswerPersister constructor.
     *
     * @param RemoveAnswerCommandHandler $removeAnswerCommandHandler
     */
    public function __construct(RemoveAnswerCommandHandler $removeAnswerCommandHandler)
    {
        $this->removeAnswerCommandHandler = $removeAnswerCommandHandler;
    }

    /**
     * @param Answer $answer
     */
    public function persist(Answer $answer): void
    {
        $removeAnswerCommand             = new RemoveAnswerCommand();
        $removeAnswerCommand->id         = $answer->id;
        $removeAnswerCommand->questionId = $answer->questionId;

        $this->removeAnswerCommandHandler->handle($removeAnswerCommand);
    }
}
