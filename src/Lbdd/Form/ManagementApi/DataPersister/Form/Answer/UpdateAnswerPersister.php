<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Answer;

use Lbdd\Form\ManagementApi\Command\Form\Answer\UpdateAnswerCommand;
use Lbdd\Form\ManagementApi\Command\Form\Answer\UpdateAnswerCommandHandler;
use Lbdd\Form\ManagementApi\DataProvider\Form\Answer\AnswerItemDataProvider;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;

class UpdateAnswerPersister
{
    /**
     * @var UpdateAnswerCommandHandler
     */
    private $updateAnswerCommandHandler;

    /**
     * @var AnswerItemDataProvider
     */
    private $answerDataItemProvider;

    /**
     * UpdateAnswerPersister constructor.
     *
     * @param UpdateAnswerCommandHandler $updateAnswerCommandHandler
     * @param AnswerItemDataProvider     $answerDataItemProvider
     */
    public function __construct(
        UpdateAnswerCommandHandler $updateAnswerCommandHandler,
        AnswerItemDataProvider $answerDataItemProvider
    ) {
        $this->updateAnswerCommandHandler = $updateAnswerCommandHandler;
        $this->answerDataItemProvider     = $answerDataItemProvider;
    }

    /**
     * @param Answer $answer
     *
     * @return Answer
     */
    public function persist(Answer $answer): Answer
    {
        $updateAnswerCommand             = new UpdateAnswerCommand();
        $updateAnswerCommand->id         = $answer->id;
        $updateAnswerCommand->value      = $answer->value;
        $updateAnswerCommand->questionId = $answer->questionId;

        $this->updateAnswerCommandHandler->handle($updateAnswerCommand);

        return $this->answerDataItemProvider->getItem(Answer::class, $answer->id);
    }
}
