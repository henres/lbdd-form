<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Answer;

use Lbdd\Form\ManagementApi\Command\Form\Answer\CreateAnswerCommand;
use Lbdd\Form\ManagementApi\Command\Form\Answer\CreateAnswerCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Answer;
use Symfony\Component\HttpFoundation\RequestStack;

class CreateAnswerPersister
{
    /**
     * @var CreateAnswerCommandHandler
     */
    private $createAnswerCommandHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * CreateQuestionPersister constructor.
     *
     * @param CreateAnswerCommandHandler $createAnswerCommandHandler
     * @param RequestStack               $requestStack
     */
    public function __construct(CreateAnswerCommandHandler $createAnswerCommandHandler, RequestStack $requestStack)
    {
        $this->createAnswerCommandHandler = $createAnswerCommandHandler;
        $this->requestStack               = $requestStack;
    }

    /**
     * @param Answer $answer
     *
     * @return Answer
     */
    public function persist(Answer $answer): Answer
    {
        $createAnswerCommand             = new CreateAnswerCommand();
        $createAnswerCommand->value      = $answer->value;
        $createAnswerCommand->formId     = $this->requestStack->getCurrentRequest()->get('form_id');
        $createAnswerCommand->questionId = $this->requestStack->getCurrentRequest()->get('question_id');

        return $this->createAnswerCommandHandler->handle($createAnswerCommand);
    }
}
