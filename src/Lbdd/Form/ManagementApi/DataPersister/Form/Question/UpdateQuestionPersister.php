<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Question;

use Lbdd\Form\ManagementApi\Command\Form\Question\UpdateQuestionCommand;
use Lbdd\Form\ManagementApi\Command\Form\Question\UpdateQuestionCommandHandler;
use Lbdd\Form\ManagementApi\DataProvider\Form\Question\QuestionItemDataProvider;
use Lbdd\Form\ManagementApi\Dto\Form\Question;

class UpdateQuestionPersister
{
    /**
     * @var UpdateQuestionCommandHandler
     */
    private $updateQuestionCommandHandler;

    /**
     * @var QuestionItemDataProvider
     */
    private $questionItemDataProvider;

    /**
     * UpdateQuestionPersister constructor.
     *
     * @param UpdateQuestionCommandHandler $updateQuestionCommandHandler
     * @param QuestionItemDataProvider     $questionItemDataProvider
     */
    public function __construct(
        UpdateQuestionCommandHandler $updateQuestionCommandHandler,
        QuestionItemDataProvider $questionItemDataProvider
    ) {
        $this->updateQuestionCommandHandler = $updateQuestionCommandHandler;
        $this->questionItemDataProvider     = $questionItemDataProvider;
    }

    /**
     * @param Question $question
     *
     * @return Question
     */
    public function persist(Question $question): Question
    {
        $updateQuestionCommand         = new UpdateQuestionCommand();
        $updateQuestionCommand->title  = $question->title;
        $updateQuestionCommand->id     = $question->id;
        $updateQuestionCommand->formId = $question->formId;

        $this->updateQuestionCommandHandler->handle($updateQuestionCommand);

        return $this->questionItemDataProvider->getItem(Question::class, $question->id);
    }
}
