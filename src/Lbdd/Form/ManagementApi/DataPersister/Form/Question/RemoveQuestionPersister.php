<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Question;

use Lbdd\Form\ManagementApi\Command\Form\Question\RemoveQuestionCommand;
use Lbdd\Form\ManagementApi\Command\Form\Question\RemoveQuestionCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Question;

class RemoveQuestionPersister
{
    /**
     * @var RemoveQuestionCommandHandler
     */
    private $removeQuestionCommandHandler;

    /**
     * RemoveQuestionPersister constructor.
     *
     * @param RemoveQuestionCommandHandler $removeQuestionCommandHandler
     */
    public function __construct(RemoveQuestionCommandHandler $removeQuestionCommandHandler)
    {
        $this->removeQuestionCommandHandler = $removeQuestionCommandHandler;
    }

    /**
     * @param Question $question
     */
    public function persist(Question $question): void
    {
        $removeQuestionCommand     = new RemoveQuestionCommand();
        $removeQuestionCommand->id = $question->id;

        $this->removeQuestionCommandHandler->handle($removeQuestionCommand);
    }
}
