<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Question;

use Lbdd\Form\ManagementApi\Command\Form\Question\CreateQuestionCommand;
use Lbdd\Form\ManagementApi\Command\Form\Question\CreateQuestionCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Form;
use Lbdd\Form\ManagementApi\Dto\Form\Question;
use Symfony\Component\HttpFoundation\RequestStack;

class CreateQuestionPersister
{
    /**
     * @var CreateQuestionCommandHandler
     */
    private $createQuestionCommandHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * CreateQuestionPersister constructor.
     *
     * @param CreateQuestionCommandHandler $createQuestionCommandHandler
     * @param RequestStack                 $requestStack
     */
    public function __construct(CreateQuestionCommandHandler $createQuestionCommandHandler, RequestStack $requestStack)
    {
        $this->createQuestionCommandHandler = $createQuestionCommandHandler;
        $this->requestStack                 = $requestStack;
    }

    /**
     * @param Question $question
     *
     * @return Question
     */
    public function persist(Question $question): Question
    {
        $createQuestionCommand         = new CreateQuestionCommand();
        $createQuestionCommand->title  = $question->title;
        $createQuestionCommand->formId = $this->requestStack->getCurrentRequest()->get('form_id');

        return $this->createQuestionCommandHandler->handle($createQuestionCommand);
    }
}
