<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Form;

use Lbdd\Form\ManagementApi\Command\Form\Form\UpdateFormCommand;
use Lbdd\Form\ManagementApi\Command\Form\Form\UpdateFormCommandHandler;
use Lbdd\Form\ManagementApi\DataProvider\Form\Form\FormItemDataProvider;
use Lbdd\Form\ManagementApi\Dto\Form\Form;

class UpdateFormPersister
{
    /**
     * @var UpdateFormCommandHandler
     */
    private $updateFormCommandHandler;

    /**
     * @var FormItemDataProvider
     */
    private $formItemDataProvider;

    /**
     * UpdateFormPersister constructor.
     *
     * @param UpdateFormCommandHandler $updateFormCommandHandler
     * @param FormItemDataProvider     $formItemDataProvider
     */
    public function __construct(
        UpdateFormCommandHandler $updateFormCommandHandler,
        FormItemDataProvider $formItemDataProvider
    ) {
        $this->updateFormCommandHandler = $updateFormCommandHandler;
        $this->formItemDataProvider     = $formItemDataProvider;
    }

    /**
     * @param Form $form
     *
     * @return Form
     */
    public function persist(Form $form): Form
    {
        $updateFormCommand        = new UpdateFormCommand();
        $updateFormCommand->title = $form->title;
        $updateFormCommand->id    = $form->id;

        $this->updateFormCommandHandler->handle($updateFormCommand);

        return $this->formItemDataProvider->getItem(Form::class, $form->id);
    }
}
