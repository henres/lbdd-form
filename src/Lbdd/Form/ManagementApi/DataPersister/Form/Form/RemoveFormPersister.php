<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Form;

use Lbdd\Form\ManagementApi\Command\Form\Form\RemoveFormCommand;
use Lbdd\Form\ManagementApi\Command\Form\Form\RemoveFormCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Form;

class RemoveFormPersister
{
    /**
     * @var RemoveFormCommandHandler
     */
    private $removeFormCommandHandler;

    /**
     * RemoveAnswerPersister constructor.
     *
     * @param RemoveFormCommandHandler $removeFormCommandHandler
     */
    public function __construct(RemoveFormCommandHandler $removeFormCommandHandler)
    {
        $this->removeFormCommandHandler = $removeFormCommandHandler;
    }

    /**
     * @param Form $form
     */
    public function persist(Form $form): void
    {
        $removeFormCommand     = new RemoveFormCommand();
        $removeFormCommand->id = $form->id;

        $this->removeFormCommandHandler->handle($removeFormCommand);
    }
}
