<?php

namespace Lbdd\Form\ManagementApi\DataPersister\Form\Form;

use Lbdd\Form\ManagementApi\Command\Form\Form\CreateFormCommand;
use Lbdd\Form\ManagementApi\Command\Form\Form\CreateFormCommandHandler;
use Lbdd\Form\ManagementApi\Dto\Form\Form;

class CreateFormPersister
{
    /**
     * @var CreateFormCommandHandler
     */
    private $createFormCommandHandler;

    /**
     * CreateAnswerPersister constructor.
     *
     * @param CreateFormCommandHandler $createFormCommandHandler
     */
    public function __construct(
        CreateFormCommandHandler $createFormCommandHandler
    ) {
        $this->createFormCommandHandler = $createFormCommandHandler;
    }

    /**
     * @param Form $form
     *
     * @return Form
     */
    public function persist(Form $form): Form
    {
        $createFormCommand        = new CreateFormCommand();
        $createFormCommand->title = $form->title;

        return $this->createFormCommandHandler->handle($createFormCommand);
    }
}
