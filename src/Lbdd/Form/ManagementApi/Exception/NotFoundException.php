<?php

namespace Lbdd\Form\ManagementApi\Exception;

use Exception;

class NotFoundException extends Exception
{
}