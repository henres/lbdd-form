<?php

namespace Lbdd\Form\ManagementApi\Exception\Form;

use Lbdd\Form\ManagementApi\Exception\NotFoundException;

class FormNotFoundException extends NotFoundException
{
}