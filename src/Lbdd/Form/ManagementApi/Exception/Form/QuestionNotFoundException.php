<?php

namespace Lbdd\Form\ManagementApi\Exception\Form;

use Lbdd\Form\ManagementApi\Exception\NotFoundException;

class QuestionNotFoundException extends NotFoundException
{
}