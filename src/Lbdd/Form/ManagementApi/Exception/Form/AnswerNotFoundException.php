<?php

namespace Lbdd\Form\ManagementApi\Exception\Form;

use Lbdd\Form\ManagementApi\Exception\NotFoundException;

class AnswerNotFoundException extends NotFoundException
{
}