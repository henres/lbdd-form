<?php

namespace Lbdd\Form\Storage\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Lbdd\Form\ManagementApi\Exception\Form\QuestionNotFoundException;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Question;
use Lbdd\Form\Storage\Form\Doctrine\Repository\QuestionRepository as QuestionDoctrineRepository;

class QuestionRepository
{
    /**
     * @var QuestionDoctrineRepository
     */
    private $questionDoctrineRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * QuestionRepository constructor.
     *
     * @param ObjectManager              $objectManager
     * @param QuestionDoctrineRepository $questionDoctrineRepository
     */
    public function __construct(ObjectManager $objectManager, QuestionDoctrineRepository $questionDoctrineRepository)
    {
        $this->objectManager              = $objectManager;
        $this->questionDoctrineRepository = $questionDoctrineRepository;
    }

    /**
     * @param string $id
     * @param string $formId
     *
     * @return Question
     * @throws QuestionNotFoundException
     */
    public function getOneByForm(string $id, string $formId): Question
    {
        $storageQuestion = $this->questionDoctrineRepository->getOneByForm($id, $formId);

        if (null === $storageQuestion) {
            throw new QuestionNotFoundException(sprintf(
                'The question "%s" does not exist in form "%s".',
                $id,
                $formId
            ));
        }

        return $storageQuestion;
    }

    /**
     * @param string $formId
     *
     * @return array
     */
    public function getAllByForm(string $formId): array
    {
        return $this->questionDoctrineRepository->findBy(
            ['form' => $formId]
        );
    }

    /**
     * @param Question $question
     *
     * @return Question
     */
    public function save(Question $question): Question
    {
        $this->objectManager->persist($question);
        $this->objectManager->flush();

        return $question;
    }

    /**
     * @param Question $question
     */
    public function delete(Question $question): void
    {
        $this->questionDoctrineRepository->deleteById($question->getId());
    }
}
