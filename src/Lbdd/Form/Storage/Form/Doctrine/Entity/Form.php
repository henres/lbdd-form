<?php

namespace Lbdd\Form\Storage\Form\Doctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Lbdd\Form\Storage\Form\Doctrine\Repository\FormRepository")
 * @ORM\Table(name="lbdd_form")
 */
class Form {
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     **/
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var Question[]
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="form")
     */
    private $questions;

    /**
     * Form constructor.
     */
    public function __construct() {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions(): ArrayCollection
    {
        return $this->questions;
    }

    /**
     * @param array $questions
     */
    public function setQuestions(array $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
}