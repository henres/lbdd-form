<?php

namespace Lbdd\Form\Storage\Form\Doctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Lbdd\Form\Storage\Form\Doctrine\Repository\AnswerRepository")
 * @ORM\Table(name="lbdd_answer")
 */
class Answer
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @var Form
     * @ORM\OneToOne(targetEntity="Form")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", nullable=true)
     */
    private $redirectToForm;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $redirectToProduct;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question): void
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return Form|null
     */
    public function getRedirectToForm(): ?Form
    {
        return $this->redirectToForm;
    }

    /**
     * @param Form $redirectToForm
     */
    public function setRedirectToForm(Form $redirectToForm): void
    {
        $this->redirectToForm = $redirectToForm;
    }

    /**
     * @return null|string
     */
    public function getRedirectToProduct(): ?string
    {
        return $this->redirectToProduct;
    }

    /**
     * @param string $redirectToProduct
     */
    public function setRedirectToProduct(string $redirectToProduct): void
    {
        $this->redirectToProduct = $redirectToProduct;
    }
}
