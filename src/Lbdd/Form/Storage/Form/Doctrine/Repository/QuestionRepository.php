<?php

namespace Lbdd\Form\Storage\Form\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Question;
use Symfony\Bridge\Doctrine\RegistryInterface;

class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * @param string $id
     *
     * @return Form|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOne(string $id): ?Question
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $id
     * @param string $formId
     *
     * @return Question|null
     */
    public function getOneByForm(string $id, string $formId): ?Question
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->where($qb->expr()->eq('u.id', ':id'))
            ->andWhere($qb->expr()->eq('u.form', ':formId'))
            ->setParameters([
                'id' => $id,
                'formId' => $formId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $id
     */
    public function deleteById(string $id): void
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->delete()
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        $qb->getQuery()->execute();
    }
}
