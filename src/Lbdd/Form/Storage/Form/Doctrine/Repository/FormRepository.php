<?php

namespace Lbdd\Form\Storage\Form\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Form::class);
    }

    /**
     * @param string $id
     *
     * @return Form|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOne(string $id): ?Form
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $id
     */
    public function deleteById(string $id): void
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->delete()
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        $qb->getQuery()->execute();
    }
}
