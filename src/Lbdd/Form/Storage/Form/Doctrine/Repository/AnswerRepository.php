<?php

namespace Lbdd\Form\Storage\Form\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Answer;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AnswerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Answer::class);
    }

    /**
     * @param string $id
     *
     * @return Form|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOne(string $id): ?Answer
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $id
     * @param string $questionId
     *
     * @return Answer|null
     */
    public function getOneByQuestion(string $id, string $questionId): ?Answer
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->where($qb->expr()->eq('u.id', ':id'))
            ->andWhere($qb->expr()->eq('u.question', ':questionId'))
            ->setParameters([
                'id'         => $id,
                'questionId' => $questionId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $id
     */
    public function deleteById(string $id): void
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->delete()
            ->where($qb->expr()->eq('u.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        $qb->getQuery()->execute();
    }
}
