<?php

namespace Lbdd\Form\Storage\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Lbdd\Form\ManagementApi\Exception\Form\FormNotFoundException;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Form;
use Lbdd\Form\Storage\Form\Doctrine\Repository\FormRepository as FormDoctrineRepository;

class FormRepository
{
    /**
     * @var FormDoctrineRepository
     */
    private $formRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * FormRepository constructor.
     *
     * @param ObjectManager          $objectManager
     * @param FormDoctrineRepository $formRepository
     */
    public function __construct(ObjectManager $objectManager, FormDoctrineRepository $formRepository)
    {
        $this->objectManager  = $objectManager;
        $this->formRepository = $formRepository;
    }

    /**
     * @param string $formId
     *
     * @return Form|null
     * @throws FormNotFoundException
     */
    public function getOne(string $formId): ?Form
    {
        $storageForm = $this->formRepository->getOne($formId);

        if (null === $storageForm) {
            throw new FormNotFoundException(sprintf('The form "%s" does not exist.', $formId));
        }

        return $storageForm;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->formRepository->findAll();
    }

    /**
     * @param Form $form
     *
     * @return Form
     */
    public function save(Form $form): Form
    {
        $this->objectManager->persist($form);
        $this->objectManager->flush();

        return $form;
    }

    /**
     * @param Form $form
     */
    public function delete(Form $form): void
    {
        $this->formRepository->deleteById($form->getId());
    }
}
