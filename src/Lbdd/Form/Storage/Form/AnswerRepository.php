<?php

namespace Lbdd\Form\Storage\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Lbdd\Form\ManagementApi\Exception\Form\AnswerNotFoundException;
use Lbdd\Form\Storage\Form\Doctrine\Entity\Answer;
use Lbdd\Form\Storage\Form\Doctrine\Repository\AnswerRepository as AnswerDoctrineRepository;

class AnswerRepository
{
    /**
     * @var AnswerDoctrineRepository
     */
    private $answerRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * AnswerRepository constructor.
     *
     * @param AnswerDoctrineRepository $answerRepository
     * @param ObjectManager            $objectManager
     */
    public function __construct(AnswerDoctrineRepository $answerRepository, ObjectManager $objectManager)
    {
        $this->answerRepository = $answerRepository;
        $this->objectManager    = $objectManager;
    }

    /**
     * @param string $answerId
     *
     * @return Answer|null
     * @throws AnswerNotFoundException
     */
    public function getOne(string $answerId): ?Answer
    {
        $storageAnswer = $this->answerRepository->getOne($answerId);

        if (null === $storageAnswer) {
            throw new AnswerNotFoundException(sprintf('The form "%s" does not exist.', $answerId));
        }

        return $storageAnswer;
    }

    /**
     * @param string $id
     * @param string $questionId
     *
     * @return null|Answer
     * @throws AnswerNotFoundException
     */
    public function getOneByQuestion(string $id, string $questionId): ?Answer
    {
        $storageQuestion = $this->answerRepository->getOneByQuestion($id, $questionId);

        if (null === $storageQuestion) {
            throw new AnswerNotFoundException(sprintf(
                'The question "%s" does not exist in question "%s".',
                $id,
                $questionId
            ));
        }

        return $storageQuestion;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->answerRepository->findAll();
    }

    /**
     * @param string $questionId
     *
     * @return array
     */
    public function getAllByQuestion(string $questionId): array
    {
        return $this->answerRepository->findBy(
            ['question' => $questionId]
        );
    }

    /**
     * @param Answer $answer
     *
     * @return Answer
     */
    public function save(Answer $answer): Answer
    {
        $this->objectManager->persist($answer);
        $this->objectManager->flush();

        return $answer;
    }

    /**
     * @param Answer $answer
     */
    public function delete(Answer $answer): void
    {
        $this->answerRepository->deleteById($answer->getId());
    }
}
