<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181114132846 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE lbdd_question (id VARCHAR(36) NOT NULL, form_id VARCHAR(36) DEFAULT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_196B8E805FF69B7D ON lbdd_question (form_id)');
        $this->addSql('CREATE TABLE lbdd_form (id VARCHAR(36) NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE lbdd_answer (id VARCHAR(36) NOT NULL, form_id VARCHAR(36) DEFAULT NULL, question_id VARCHAR(36) DEFAULT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, redirect_to_product VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FF76D6F5FF69B7D ON lbdd_answer (form_id)');
        $this->addSql('CREATE INDEX IDX_FF76D6F1E27F6BF ON lbdd_answer (question_id)');
        $this->addSql('ALTER TABLE lbdd_question ADD CONSTRAINT FK_196B8E805FF69B7D FOREIGN KEY (form_id) REFERENCES lbdd_form (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lbdd_answer ADD CONSTRAINT FK_FF76D6F5FF69B7D FOREIGN KEY (form_id) REFERENCES lbdd_form (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lbdd_answer ADD CONSTRAINT FK_FF76D6F1E27F6BF FOREIGN KEY (question_id) REFERENCES lbdd_question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE lbdd_answer DROP CONSTRAINT FK_FF76D6F1E27F6BF');
        $this->addSql('ALTER TABLE lbdd_question DROP CONSTRAINT FK_196B8E805FF69B7D');
        $this->addSql('ALTER TABLE lbdd_answer DROP CONSTRAINT FK_FF76D6F5FF69B7D');
        $this->addSql('DROP TABLE lbdd_question');
        $this->addSql('DROP TABLE lbdd_form');
        $this->addSql('DROP TABLE lbdd_answer');
    }
}
